// This header file includes a device specific header depending on the
// value passed to the compiler by the -mmcu= option
#include <msp430.h>

// definitions for the TI Launchpad MSP-EXP430G2
#define LED1    BIT0
#define LED2    BIT6
#define LED_DIR P1DIR
#define LED_OUT P1OUT

void init_leds() {
    // configure LED pins as output
    LED_DIR |= LED2 | LED1;
    // turn on LED1
    LED_OUT |= LED1;
    // turn off LED2
    LED_OUT &= ~LED2;   
}

int main() {
    // disable watchdog timer
    // WDTPW has to be present when writing to WDTCTL
    WDTCTL = WDTPW | WDTHOLD;

    // set up MCLK,SMCLK and ACLK to use VLOCLK
    BCSCTL2 = SELM1 | SELM0 | SELS;
    BCSCTL3 = LFXT1S1;
   
    init_leds();

    // set up Timer A
    // for VLOCLK = 12kHz this will overflow every 500ms
    TACCR0 = 5999;
    // start timer
    TACTL = TASSEL0 | MC0;
    // enable timer interrupt
    TACCTL0 = CCIE;

    // interrupts have to be globally enabled
    // this function is defined in in430.h
    _enable_interrupts();

    // enter low power mode with only ACLK active
    _low_power_mode_3();

    while(1);
    return 0;
}

/*
This syntax is used to define interupt service routines in msp430-gcc
The interrupt vector names (e.g. TIMERA0_VECTOR) are defined in the device
specific header file (e.g. msp430g2231.h) included from msp430.h
cf. TI document SLAU646E section 4.5 MSP430 GCC Interrupts Definition
*/
void __attribute__ ((interrupt(TIMERA0_VECTOR))) TIMERA0_ISR(void) {
    // toggle LEDs
    LED_OUT ^= LED2 | LED1;
}
