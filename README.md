# MSP430 example project

This repository contains an TI MSP430 example project that uses the MSP430-GCC 
opensource compiler for MSP430 embedded software development.


## Building using CMake
This project uses CMake for generating the Makefile. After CMake configuration
the project can be built using make.

```sh
# make a build directory and switch to it
$ mkdir build && cd build
# create Makefile using CMake
$ cmake -DCMAKE_TOOLCHAIN_FILE=CMakeToolchain ../
# actually build the project
$ make
```

## Flashing
```sh
$ mspdebug rf2500 prog build/main.elf
```

## Debugging using GDB
In one terminal start the debug server
```sh
$ mspdebug rf2500 "gdb"
```

In another start gdb
```sh
$ gdb -ex 'target remote :2000' build/main.elf
```

The included source file toggles the LEDs on the TI MSP-EXP430G2 Launchpad.
